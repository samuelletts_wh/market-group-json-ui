# MarketsJsonUi

This project was created to modify football-market-group-grouping.json in a UI.

## How to Run

`nvm use`
`npm i`
`npm start`
Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

Json file will automatically load from content stack, or you can overwrite it and load your own using the load JSON button. Once happy with the changes (or just want to save your work) you can export json
