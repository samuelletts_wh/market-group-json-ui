import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {
  CdkDrag,
  CdkDragDrop, CdkDragStart,
  CdkDropList,
  CdkDropListGroup,
  DragDropModule,
  moveItemInArray,
  transferArrayItem
} from '@angular/cdk/drag-drop';
import marketGroupGroupingsJson from '../assets/football-market-group-grouping.json';
import { NgClass } from '@angular/common';
import { MatButton, MatIconButton } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIcon } from '@angular/material/icon';
import { HttpClient } from '@angular/common/http';
import { MatButtonToggle } from '@angular/material/button-toggle';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import data from '../assets/football-market-group-grouping.json';
import { MatCheckbox } from '@angular/material/checkbox';

interface Market {
  name: string,
  position: number,
  inplay?: boolean,
  merged?: any[]
}

interface Collection {
  name: string,
  markets: Market[],
  position: number
}

interface MarketGroupingsJson {
  collections: Collection[]
}

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CdkDropList, CdkDropListGroup, DragDropModule, NgClass, MatButton, MatMenuModule, MatIcon, MatIconButton, MatButtonToggle, MatSlideToggle, MatCheckbox],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {
  title = 'markets-json-ui';

  activeMarketGroupingIndex: number = 0;
  activeMarketIndex: number = 0;
  data: MarketGroupingsJson = marketGroupGroupingsJson;
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  ngOnInit() {
    console.log(this.data);
    this.http.get<MarketGroupingsJson>('https://retail-content-api-dev.retail.aws-eu-west-1.dev.williamhill.plc/content/json/FootballMarketGroupGroupings/default/default').subscribe(
      data => this.data = data
    )
  }

  setActiveMarketGrouping(index: number) {
    this.activeMarketGroupingIndex = index;
  }

  setActiveMarket($index: number) {
    this.activeMarketIndex = $index;
  }

  //drag and drop
  drop(event: CdkDragDrop<any[], any>) {
    const active = this.data.collections[this.activeMarketGroupingIndex].name;
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      console.log(event.container.data);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
      console.log(this.data);
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    }
    this.activeMarketGroupingIndex = this.data.collections.findIndex(item => item.name === active);
  }

  canDrop(item: CdkDrag, list: CdkDropList) {
    return item.dropContainer.id === list.id;
  }

  public activeMarketDragIndex: number = -1;
  public activeMarketDrag: Market | null = null;
  private marketCollectionHoverIndex: number = -1;

  onDragMarket(item: Market, index: number) {
    this.activeMarketDrag = item;
    this.activeMarketDragIndex = index;
  }

  onDragMarketEnd(item: Market) {
    if (this.marketCollectionHoverIndex >= 0 && this.activeMarketDragIndex >= 0) {
      this.data.collections[this.activeMarketGroupingIndex].markets.splice(this.activeMarketDragIndex, 1);
      this.data.collections[this.marketCollectionHoverIndex].markets.unshift(item);
      this.setActiveMarketGrouping(this.marketCollectionHoverIndex);
    }
    this.activeMarketDrag = null;
    this.activeMarketDragIndex = -1;
    this.marketCollectionHoverIndex = -1;
  }

  mouseEnterActiveMarketCollection(index: number, activeMarketGrouping: any) {
    if (this.activeMarketDrag) {
      this.marketCollectionHoverIndex = index;
      activeMarketGrouping.classList.add('hoverActive');
    }
  }

  mouseLeaveActiveMarketCollection(index: number, activeMarketGrouping: any) {
    this.marketCollectionHoverIndex = -1;
    activeMarketGrouping.classList.remove('hoverActive');
  }

  onExportJson() {
    this.data.collections.forEach((item, index) => {
      item.position = index;
      item.markets.forEach((item, index) => {
        item.position = index;
      })

    });

    //position all correctly
    this.data.collections.forEach((collection, index) => {
      collection.position = index;

      let i = 0;
      collection.markets.forEach((market) => {
        market.position = i++;
        market.merged?.forEach(mergedMarket => {
          mergedMarket.position = i++;
        })
      });
    });

    const jsonString = JSON.stringify(this.data);
    const blob = new Blob([jsonString], {type: 'application/json'});
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'football-market-group-grouping.json';
    a.click();
    window.URL.revokeObjectURL(url);
  }

  onExportCsv() {
    // Empty array for storing the values
    let csvRows = [];
    let markets = this.data.collections.map(col => col.markets);
    const numberOfCols = markets.length;
    console.log(markets, numberOfCols);
    let numberOfRows = 0;
    markets.forEach(market => numberOfRows = market.length > numberOfRows ? market.length : numberOfRows);
    console.log(numberOfRows);
    const headers: string[] = this.data.collections.map(col => col.name);
    csvRows.push(headers.join(','));


    for (let row = 0; row < numberOfRows; row++) {
      const rowData = [];
      for (let col = 0; col < numberOfCols; col++) {
        rowData.push(this.data.collections[col].markets[row] ? `"${this.data.collections[col].markets[row]?.name}"` : '');
      }
      console.log(rowData);
      csvRows.push(rowData.join(','));
    }

    console.log(csvRows);

    // Returning the array joining with new line
    const join = csvRows.join('\n');
    // Create a Blob with the CSV data and type
    const blob = new Blob([join], {type: 'text/csv'});

    // Create a URL for the Blob
    const url = URL.createObjectURL(blob);

    // Create an anchor tag for downloading
    const a = document.createElement('a');

    // Set the URL and download attribute of the anchor tag
    a.href = url;
    a.download = 'download.csv';

    // Trigger the download by clicking the anchor tag
    a.click();
  }


  //editing list
  onAddMarketGroup() {
    let marketGroupName = prompt("Please enter market group name", "")
    if (marketGroupName) {
      this.data.collections.push({"name": marketGroupName, "position": this.data.collections.length, "markets": []})
    }
    console.log(this.data);
  }

  onAddMarket() {
    let marketName = prompt("Please enter market group name", "")
    if (marketName) {
      this.data.collections[this.activeMarketGroupingIndex].markets.unshift({
        "name": marketName,
        "position": 0
      })
    }
  }

  onEditMarketGroupName(index: number) {
    const newMarketGroupName = prompt("Please enter new market group name", "");
    if (newMarketGroupName) {
      this.data.collections[index].name = newMarketGroupName;
    }
  }

  onEditMarketName(index: number) {
    const newMarketName = prompt("Please enter new market name", "");
    if (newMarketName) {
      this.data.collections[this.activeMarketGroupingIndex].markets[index].name = newMarketName;
    }
  }

  onDeleteMarketGroup(marketGroupName: string) {
    const confirmation = confirm(`Are you sure you want to delete the market group "${marketGroupName}"? This will delete all markets within the group.`);
    if (confirmation) {
      this.activeMarketGroupingIndex = 0;
      const index = this.data.collections.findIndex((group) => group.name === marketGroupName);
      if (index !== -1) {
        this.data.collections.splice(index, 1);
        console.log(`Market group "${marketGroupName}" deleted.`);
      } else {
        console.log(`Market group "${marketGroupName}" not found.`);
      }
    }
  }

  onDeleteMarket($index: number) {
    this.data.collections[this.activeMarketGroupingIndex].markets.splice($index, 1);
  }

  onDeleteMerged($index: number) {
    this.data.collections[this.activeMarketGroupingIndex].markets[this.activeMarketIndex].merged!.splice($index, 1);
  }

  onJsonLoad(event: any) {
    const file: File = event.target!.files[0];
    const fileReader = new FileReader();
    fileReader.readAsText(file, "UTF-8");
    fileReader.onload = () => {
      this.data = JSON.parse(fileReader.result as string);
      this.data.collections.forEach((collection, index) => {
        collection.position = index;
        collection.markets.sort((a, b) => a.position-b.position);
        collection.markets.forEach(market => market.merged?.sort((a, b) => a.position-b.position));
      });
    }
    fileReader.onerror = (error) => {
      console.log(error);
    }
  }
}
